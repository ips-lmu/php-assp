PHP_ARG_ENABLE(assp,
	[Whether to enable the "assp" extension],
	[  --enable-assp      Enable "assp" extension support])

export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

if test $PHP_ASSP != "no"; then
	PHP_REQUIRE_CXX()
	PHP_SUBST(ASSP_SHARED_LIBADD)
	PHP_ADD_LIBRARY(stdc++, 1, ASSP_SHARED_LIBADD)

	LIBASSP_LIBS=`pkg-config --libs assp`
	LIBASSP_INC=`pkg-config --cflags assp`

	PHP_CHECK_LIBRARY(assp, printKSVrefs,
	[
		PHP_EVAL_INCLINE($LIBASSP_INC)
		PHP_EVAL_LIBLINE($LIBASSP_LIBS, ASSP_SHARED_LIBADD)
	],[
		AC_MSG_ERROR([libassp not found. Check config.log for more information.])
	]
	)

	PHP_NEW_EXTENSION(assp, assp.cpp, $ext_shared)
fi

