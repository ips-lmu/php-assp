#ifndef PHP_ASSP_H
#define PHP_ASSP_H

#define PHP_ASSP_EXTNAME  "assp"
#define PHP_ASSP_EXTVER   "0.1"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif 

extern "C" {
#include "php.h"
#include <assp.h>
#include <asspfio.h>
#include <dataobj.h>
#include <ksv.h>
}

extern zend_module_entry ASSP_module_entry;
#define phpext_ASSP_ptr &assp_module_entry;

#endif /* PHP_ASSP_H */
