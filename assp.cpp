#include "php_assp.h"

ZEND_MINIT_FUNCTION(assp)
{
	return SUCCESS;
}

ZEND_FUNCTION(assp_ksvf0) {
	
	//////////
	// Local variables
	// 
	
	// This is the input we receive from the calling PHP code
	char *input = NULL;
	size_t input_len;
	
	// Analysis options
	AOPTS anaOpts;

	// These are the Data Objects we need in order to talk to libassp
	DOBJ *inputDOp, *outputDOp;

	//
	//////////


	//////////
	// Initialisation
	//

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &input, &input_len) == FAILURE) {
		RETURN_NULL();
	}

	// Default analysis options
	setKSVdefaults(&anaOpts);
	
	// Allocate data object and associate it with in-memory buffer
	inputDOp = allocDObj();
	if (inputDOp == NULL) {
		RETURN_NULL();
	}
	inputDOp->openMode = AFO_READ;
	inputDOp->filePath = "in-memory"; // fake filename

	inputDOp->fp = fmemopen (input, input_len, "rb");
	
	if (inputDOp->fp == NULL) {
		RETURN_NULL();
	}

	//
	//////////
	

	//////////
	// Have libassp parse the header
	//
	
	inputDOp->fileFormat = FF_WAVE;

	if (getHeader(inputDOp) != 0) {
		// a return value of 1 indicates "warnings only", but we will
		// drop out anyway
		RETURN_NULL();
	}

	//
	//////////


	///////////
	// Actual computation

	// createKSV() allocates an empty object to hold the f0 result
	outputDOp = createKSV (inputDOp, &anaOpts);
	if (outputDOp == NULL) {
		RETURN_NULL();
	}

	if (computeKSV (inputDOp, &anaOpts, outputDOp, NULL) == NULL) {
		RETURN_NULL();
	}

	array_init(return_value);

	for (int i = 0; i < outputDOp->bufNumRecs; ++i) {
		add_next_index_double(
			return_value,
			((float *) outputDOp->dataBuffer)[i]
		);
	}

	//
	//////////

	fclose(inputDOp->fp);
	freeDObj(inputDOp);
	freeDObj(outputDOp);
}

zend_function_entry assp_functions[] = {
	ZEND_FE(assp_ksvf0, NULL)
	{NULL, NULL, NULL}
};

zend_module_entry assp_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	PHP_ASSP_EXTNAME,
	assp_functions,        /* Functions */
	ZEND_MINIT(assp),
	NULL,                  /* MSHUTDOWN */
	NULL,                  /* RINIT */
	NULL,                  /* RSHUTDOWN */
	NULL,                  /* MINFO */
#if ZEND_MODULE_API_NO >= 20010901
	PHP_ASSP_EXTVER,
#endif
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_ASSP
extern "C" {
ZEND_GET_MODULE(assp)
}
#endif
