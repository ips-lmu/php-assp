# php-assp

This is a proof-of-concept PHP extension to show that we can create PHP bindings
to Michel Scheffer’s [libassp](https://sourceforge.net/projects/libassp/).

It currently only supports the `computeKSV()` function, which is an
implementation of Kurt Schafer-Vincent’s pitch detection algorithm.

## How to compile

libassp must be installed on your system. It may be installed in `/usr/local`.

After cloning the repository, execute these commands:

```shell
$ phpize
$ ./configure --enable-assp
$ make
```

You can then run PHP with the newly compiled extension:

```shell
$ php -d"extension=$(pwd)/modules/assp.so" myScript.php
```

The PHP script will have the function assp_ksvf0() at its disposal, which takes
a string parameter containing an audio file (the contents, not the file name).
It returns an array of floats that contains the audio file’s fundamental
frequency as a function of time.
